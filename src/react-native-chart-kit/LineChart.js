import React, {useState} from 'react';
import {LineChart} from 'react-native-chart-kit';
import {Dimensions, View, Text, TouchableOpacity} from 'react-native';

const LineChartComponent = () => {
  const [labels, setLabels] = useState(['One']);
  const [data, setData] = useState([1])

  const setChartData = () => {
    setLabels(['One', 'Two', 'Three'])
    setData([Math.random() * 100, Math.random() * 100, Math.random() * 100, Math.random() * 100, Math.random() * 100, Math.random() * 100, Math.random() * 100, Math.random() * 100, Math.random() * 100])
  }

  return (
      <View>
        <TouchableOpacity onPress={() => setChartData()}>
          <Text>
            Bezier Line Chart
          </Text>
        </TouchableOpacity>
        <LineChart
          data={{
            labels,
            datasets: [{data}]
          }}
          width={Dimensions.get('window').width - 20} // from react-native
          height={200}
          yAxisLabel={'$'}
          chartConfig={{
            backgroundColor: 'orange',
            backgroundGradientFrom: 'yellowgreen',
            backgroundGradientTo: 'blue',
            decimalPlaces: 2, // optional, defaults to 2dp
            color: (opacity = .5) => `rgba(255, 255, 255, ${opacity})`,
            style: {
              borderRadius: 16
            }
          }}
          style={{
            marginVertical: 8,
            borderRadius: 16
          }}
        />
      </View>
  )
}


export default LineChartComponent;