import React from 'react';
import {StackedBarChart} from 'react-native-chart-kit';
import {Dimensions} from 'react-native';

const StackedBarChartComponent = () => {
  const data ={
  labels: ['Test1', 'Test2'],
  legend: ['L1', 'L2', 'L3'],
  data: [
    [60, 60, 60],
    [30,30,60],
  ],
  barColors: ['#dfe4ea', '#ced6e0', '#a4b0be'],
 }
 
  return (
    <StackedBarChart 
      data={data}
      width={Dimensions.get('window').width - 20} 
      height={220}
      accessor="population"
      chartConfig={{
            backgroundColor: 'orange',
            backgroundGradientFrom: 'yellowgreen',
            backgroundGradientTo: 'blue',
            color: (opacity = .5) => `rgba(0, 0, 0, ${opacity})`,
          }}
      style={{
        marginTop: 10
      }}
    />
      
  )
}

export default StackedBarChartComponent;
  