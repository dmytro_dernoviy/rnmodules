import React from 'react';
import {ProgressChart} from 'react-native-chart-kit';
import {Dimensions} from 'react-native';

const ProgressChartComponent = () => {
  const data = {
    labels: ['Swim', 'Bike', 'Run'], // optional
    data: [0.4, 0.6, 0.8]
}
  return (
    <ProgressChart 
      data={data}
      width={Dimensions.get('window').width - 20} 
      height={220}
      chartConfig={{
            backgroundColor: 'orange',
            backgroundGradientFrom: 'yellowgreen',
            backgroundGradientTo: 'blue',
            decimalPlaces: 2, // optional, defaults to 2dp
            color: (opacity = .5) => `rgba(0, 0, 0, ${opacity})`,
            style: {
              borderRadius: 16
            }
          }}/>
      
  )
}

export default ProgressChartComponent;
  