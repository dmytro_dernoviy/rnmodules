import React from 'react';
import {AdMobBanner} from 'react-native-admob';
import {View} from 'react-native';

const AdMob = () => 
    <View style={{position: 'absolute', bottom: 10}}>
      <AdMobBanner
        adSize="banner"
        adUnitID="ca-app-pub-3940256099942544/6300978111"
        testDevices={[AdMobBanner.simulatorId]}
        onAdFailedToLoad={error => console.error(error)}
      />
    </View>

export default AdMob;