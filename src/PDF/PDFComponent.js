import React, {useState} from 'react';
import { StyleSheet, Dimensions, View } from 'react-native';
import {ActivityIndicator, TouchableOpacity, Text} from 'react-native';
 
import Pdf from 'react-native-pdf';
 
const PDFComponent  = () => {
        const source = {uri:'http://samples.leanpub.com/thereactnativebook-sample.pdf'};
        const [pageNumber, setPageNumber] = useState(1)

        const incrementPage = () => {
          setPageNumber(pageNumber + 1)
        }

        const decrementPage = () => {
          if(pageNumber > 1) {
            setPageNumber(pageNumber - 1)
          }
        }

        return (
            <View style={styles.container}>
              <TouchableOpacity onPress={() => incrementPage()}>
                <Text>Next Page</Text>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => decrementPage()}>
                <Text>Prev Page</Text>
              </TouchableOpacity>
              <Pdf
                source={source}
                style={styles.pdf}
                activityIndicator={<ActivityIndicator size='large' color="yellowgreen"/>}
                page={pageNumber}
              />
            </View>
        )
  }
 
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'center',
        marginTop: 25,
    },
    pdf: {
        flex:1,
        width:Dimensions.get('window').width,
        height:Dimensions.get('window').height,
    }
});

export default PDFComponent;